// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract RevertAssertRequire {
    int public value;

    constructor() { value = 0; }

    event Incremented(int v);
    event Decremented(int v);

    function revert0() public {
        value += 1;
        revert();
    }

    function revert1() public {
        value += 1;
        revert("message_of_revert");
    }

    function assert1() public {
        value -= 1;
        assert(0 == 1);
    }

    function require1() public {
        value -= 1;
        require(0 == 1);
    }

    function require2() public {
        value -= 1;
        require(0 == 1, "required_0_==_1_to_be_equal");
    }
}
