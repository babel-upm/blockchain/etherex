// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Store {
    int public value;

    constructor(int v) {
        value = v;
    }

    event Set(int v);

    function set(int v) public {
        value = v;
        emit Set(v);
    }

    function get() public view returns (int) {
        return value;
    }
}
