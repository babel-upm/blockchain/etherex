defmodule Etherex.Client.EthnodeGeth do
  use Etherex.Client, command: "ethnode", args: "geth", killall: "geth"
end
