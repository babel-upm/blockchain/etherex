// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import './Ping.sol';

contract PingPong {

    Ping p = new Ping();

    function ping() public {
        p.ping();
    }
}
