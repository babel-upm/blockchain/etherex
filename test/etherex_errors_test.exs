defmodule EtherexErrorsTest do
  require Logger
  use ExUnit.Case

  test "revert assert require" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/RevertAssertRequire.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call(instance, owner, "revert0", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert message_of_revert", _hash} =
             Etherex.call(instance, owner, "revert1", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call(instance, owner, "assert1", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call(instance, owner, "require1", [], gas: 1_000_000)

    assert {:error,
            "VM Exception while processing transaction: revert required_0_==_1_to_be_equal",
            _hash} = Etherex.call(instance, owner, "require2", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call_transaction(instance, owner, "revert0", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert message_of_revert", _hash} =
             Etherex.call_transaction(instance, owner, "revert1", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call_transaction(instance, owner, "assert1", [], gas: 1_000_000)

    assert {:error, "VM Exception while processing transaction: revert", _hash} =
             Etherex.call_transaction(instance, owner, "require1", [], gas: 1_000_000)

    assert {:error,
            "VM Exception while processing transaction: revert required_0_==_1_to_be_equal",
            _hash} = Etherex.call_transaction(instance, owner, "require2", [], gas: 1_000_000)
  end
end
