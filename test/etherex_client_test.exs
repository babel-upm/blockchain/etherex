defmodule EtherexClientTest do
  require Logger
  use ExUnit.Case

  test "already running" do
    case Application.fetch_env!(:etherex, :test_client) do
      nil ->
        Logger.info("Network manually started")
        assert true

      client ->
        case client.start() do
          :ok ->
            client.stop()
            flunk("Tests were running with no Ethereum client started")

          {:error, {:already_started, _}} ->
            assert true

          {:error, "Fail starting" <> _} ->
            assert true
        end
    end
  end
end
