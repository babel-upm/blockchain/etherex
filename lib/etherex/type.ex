defmodule Etherex.Type do
  @moduledoc """
  Basic types in Ethereum.
  """

  @typedoc """
  20 bytes account addresses in hex representation (starting with `"0x"`)
  """
  # 366 = 8 * (2 + 40 * 2)
  @type address() :: <<_::336>>

  @typedoc """
  32 bytes hashes in hex representation (starting with `"0x"`)
  """
  # 528 = 8 * (2 + 32 * 2)
  @type hash :: <<_::528>>

  @typedoc """
  Numbers, byte arrays, account addresses, hashes, and bytecode
  arrays encoded as hex, prefix with “0x”, two hex digits per byte.
  """
  @type hex() :: <<_::16, _::_*8>>

  @typedoc """
  Tags to represent the default block in some methods.
  """
  @type tag() :: :earliest | :latest | :pending

  @typedoc """
  Quantities as non negative integers (used in values, block height,
  counts, etc.)
  """
  @type quantity() :: non_neg_integer()

  @typedoc """
  Ether units
  """
  @type unit() :: :WEI | :GWEI | :PWEI | :ETH

  @typedoc """
  Events emitted in a new message call transcation.
  """
  @type event() :: {String.t(), %{String.t() => any()}}

  @typedoc """
  Errors: `:econnrefused` when no client is listening, otherwise, when
  the client encounters an error a short description of the error
  """
  @type error() :: :econnrefused | :timeout | String.t()

  @typedoc """
  Block parameters are block numbers or a tag.
  """
  @type block_parameter() :: quantity() | tag()

  @typedoc """
  Options that can be used in some Etherex function calls.
  """
  @type opts() :: [
          {:gas, quantity()}
          | {:gas_price, quantity()}
          | {:nonce, quantity()}
        ]

  @typedoc """
  Call error contains the hash of the transaction, the error type (for example revert) and the
  reason.
  """
  @type call_error :: %{hash: hash(), error: String.t(), reason: String.t()}
end
