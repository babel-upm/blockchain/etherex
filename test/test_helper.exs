require Logger

case Application.fetch_env!(:etherex, :test_client) do
  nil ->
    ExUnit.start()

  client ->
    case client.start() do
      :ok ->
        ExUnit.after_suite(fn _ ->
          client.stop()
        end)

        ExUnit.start()

      _error ->
        Logger.error("Fail starting the Ethereum client, check error messages.")

        Logger.flush()
        System.halt(1)
    end
end
