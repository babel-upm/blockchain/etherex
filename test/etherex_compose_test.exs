defmodule EtherexComposeTest do
  require Logger
  use ExUnit.Case

  test "counter store" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/CounterStore.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    creation_hash = Etherex.get_contract_creation_hash(instance)

    assert {:ok, [{"Components", %{"store" => store_instance, "counter" => counter_instance}}]} =
             Etherex.get_events(instance, creation_hash)

    assert {:ok, 0} = Etherex.get_balance(store_instance)
    assert {:ok, 0} = Etherex.get_balance(counter_instance)

    n = 10

    for i <- 1..n do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "set", [i], gas: 1_000_000)

      assert {:ok, events} = Etherex.get_events(instance, hash)

      assert Enum.member?(events, {"Set", %{"v" => i, "n" => i}}),
             "Events: #{inspect(events)}"
    end

    for i <- 1..n do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "unset", [], gas: 1_000_000)
      j = n - i

      assert {:ok, events} = Etherex.get_events(instance, hash)

      assert Enum.member?(events, {"Unset", %{"v" => n, "n" => j}}),
             "Events: #{inspect(events)}"
    end

    assert {:error, "VM Exception while processing transaction: revert counter is not positive",
            _hash} = Etherex.call_transaction(instance, owner, "unset", [], gas: 1_000_000)
  end
end
