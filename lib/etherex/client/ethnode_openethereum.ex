defmodule Etherex.Client.EthnodeOpenethereum do
  use Etherex.Client, command: "ethnode", args: "openethereum", killall: "openethereum"
end
