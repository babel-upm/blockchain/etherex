defmodule EtherexTimeTest do
  use ExUnit.Case

  require Logger

  import ExUnit.Assertions

  test "mine works" do
    client = Application.fetch_env!(:etherex, :test_client)

    clients_with_mine = [Etherex.Client.GanacheCLI]

    if Enum.member?(clients_with_mine, client) do
      for _ <- 1..10 do
        assert {:ok, bn} = Etherex.Time.mine()
        assert is_integer(bn)
      end
    else
      assert {:error, error} = Etherex.Time.mine()
      assert is_binary(error)
      Logger.warning("Etherex.Time.mine not supported by client #{client}: #{error}")
    end
  end
end
