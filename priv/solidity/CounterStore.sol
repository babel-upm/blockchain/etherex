// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Counter.sol";
import "./Store.sol";

contract CounterStore {
    Store store;
    Counter counter;

    event Components(Store store, Counter counter);
    event Got(int v, int n);
    event Set(int v, int n);
    event Unset(int v, int n);

    constructor() {
        counter = new Counter();
        store = new Store(0);
        emit Components(store, counter);
    }

    function set(int v) public {
        store.set(v);
        counter.inc();
        emit Set(store.get(), counter.get());
    }

    function unset() public {
        counter.dec();
        emit Unset(store.get(), counter.get());
    }

    function get() public {
        emit Got(store.get(), counter.get());
    }
}
