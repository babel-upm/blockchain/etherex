defmodule Etherex.Client.Geth do
  use Etherex.Client,
    command: "geth",
    args: "--dev --http --http.api \"eth,net,web3\"",
    killall: "geth"
end
