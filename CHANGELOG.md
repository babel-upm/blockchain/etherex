# Etherex Changelog

## Legend

- `[+]` Added: for new features
- `[-]` Removed: for now removed features
- `[C]` Changed: for changes in existing functionality
- `[F]` Fixed: for any bug fixes
- `[O]` Obsolete: for soon-to-be removed features
- `[T]` Technical: change that doesn't affect the API (eg. refactoring, tooling, etc.)

## Next release

## 1.1.0

- [+] New module Etherex.Contract.Manager: an abstraction layer over
  the default Etherex contract API to allow clients to interact with
  contracts only using addresses and transaction hashes.

## 1.0.0

- [T] No changes, just the bump to avoid problems with the releases versions in hex.pm.

## 1.0.0-rc19

- [T] ex_abi dependency updated to 0.6.0.
- [T] Preparing etherex to be published in hex.pm

## 1.0.0-rc18

- [T] Refactor of tests in order to test different nodes (Geth, Openethereum, Ethnode, etc.).
- [F] Some fixed decoding errors (decode_json_rpc_error).
- [+] Extracting the hash from errors when a hash exists.
- [+] A bit of inverse engineering for discovering some formats in different networks (Openethereum or Geth).

## 1.0.0-rc17

- [F] Fix: decoding Events from logs fails when events in the log are not events from the given contract.
- [C] Fix: decoding RPC errors stopped working after upgrading ex_abi.
- [T] Library ex_abi updated to 5.6
- [+] New operations added: minor_start, minor_stop and evm_snapshot (in Etherex.Time)

## 1.0.0-rc16

- [T] More tests: contracts as components of contracts.
- [F] Error decoding improved.
- [+] Default block parameter improved and added to all functions where it is relevant.
- [+] Etherex.syncing function added.

## 1.0.0-rc15

- [F] Error fixed extracting events of a contract.

## 1.0.0-rc14

- [C] Removed flag allowUnlimitedContractSize in GanacheCLI.
- [F] gasLimit flag in Ganache limited to max safe integer (9007199254740991).
- [+] Case {:error, :timeout} added to decode_json_rpc_error.
- [T] Command line in lib/etherex/client.ex constructed at compile time.

## 1.0.0-rc13

- [C] Flag --optimize added to Solidity compiler.

## 1.0.0-rc12

- [+] New clients: Openethereum and Geth (not with ethnode).
- [T] Specific arguments for starting Ethereum clients.
- [T] Tests for Etherex.Time.mine

## 1.0.0-rc11

- [C] EthNode removed as a client, Openethereum and Geth are now
  clients (although for the moment both are started with ethnode).
- [C] Some error messages in logs when starting a client are now warnings.

## 1.0.0-rc10

- [F] Some functions to return {:ok, nil} when the request does not
  fail but information is not processed yet. Eg. functions to get
  receipts, gas consumption, etc.
- [T] New interesting tests to check that development clients mine the
  block after transactions.
- [F] If Contract.get_events and Contract.get_contract_address to
  return {:ok, nil} if transaction is pending.
- [F] Fixing some error decodings in order to honor the type error().

## 1.0.0-rc9

- [F] Avoiding match error when a client is started and start fails.

## 1.0.0-rc8

- [C] Function renamed: Etherex.get_creation_hash -> Etherex.get_contract_creation_hash
- [C] Function renamed: Etherex.get_creation_address -> Etherex.get_contract_address
- [C] Function renamed: Contract.get_creation_address -> Contract.get_address

## 1.0.0-rc7

- [+] Functions related to contracts to get bytecode, creator, the has
  of the creationg transaction and address of the contract.
- [C] The names of some types has changed: compiled_code() -> bytecode()
- [T] Very strong refactor: Etherex.CompiledCode moved to Contract,
  bytecode and contract instance are now actually opaque (includes
  some accessor).
- [T] Added a new module Etherex.Type with the basic types: address,
  hash, hex, tag, quantity, and unit.

## 1.0.0-rc6

- [T] Tests added for gas cost and gas cost estimation.
- [+] Convenient function added: Etherex.estimate_gas_cost.

## 1.0.0-rc5

- [+] Convenient function added: Etherex.gas_cost
- [+] Added methods Etherex.estimate_gas
- [T] README improved, mainly TODO entries added.

## 1.0.0-rc4

- [+] New public module Etherex.Time with functions to simulate time
  events, for the moment "mine" to force a block to be mined.
- [T] Refactoring: moving some private functions from Etherex to
  Etherex.Helpers (to be used by other modules).
- [+] :latests as default parameter of Etherex.get_block/1.

## 1.0.0-rc3

- [C] Minor change in solidity version to compile examples.
- [+] Ethereum clients as gen servers: API changed.
- [F] Added a case of error when the RPC error cannot be JSON decoded.

## 1.0.0-rc2

- [+] Modules to start/stop Ethereum clients: Etherex.Client.{EthNode, GanacheCLI}

## 1.0.0-rc1

- [+] Initial release with the following types and functions in module `Etherex`:
    - Types:
        address/0
        compiled_code/0
        contract/0
        error/0
        event/0
        hash/0
        quantity/0
        tag/0
        unit/0
    - Functions
        accounts/0
        accounts!/0
        block_number/0
        block_number!/0
        call/5
        call!/5
        call_transaction/5
        call_transaction!/5
        client_version/0
        client_version!/0
        compile_solidity/1
        compile_solidity!/1
        deploy/4
        deploy!/4
        gas_price/0
        gas_price!/0
        get_balance/1
        get_balance!/1
        get_block/1
        get_block!/1
        get_events/2
        get_events!/2
        get_transaction/1
        get_transaction!/1
        get_transaction_receipt/1
        get_transaction_receipt!/1
        net_listening/0
        net_listening!/0
        net_version/0
        net_version!/0
        protocol_version/0
        protocol_version!/0
        transfer/4
        wei_to_decimal/2
        wei_to_string/2
