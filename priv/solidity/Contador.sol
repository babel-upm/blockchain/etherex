// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract Counter {
    int public value;

    constructor() { value = 0; }

    event Result(int v);

    function inc() public {
        value += 1;
        emit Result(value);
    }

    function dec() public {
        require(value > 0, "counter is not positive");
        value -= 1;
        emit Result(value);
    }

    function get() public view returns (int256) {
        return value;
    }

}
