import Config

# We use Ganache by default, use
# - nil if you want to start the client manualy
# - Etherex.Client.GanacheCLI to start Ganache-cli
# - Etherex.Client.EthnodeOpenethereum to start Openethereum (with ethnode)
# - Etherex.Client.EthnodeEthnodeGeth to start Geth (with ethnode)
# - Etherex.Client.Openethereum to start Openethereum
# - Etherex.Client.Geth to start Geth
config :etherex, test_client: Etherex.Client.GanacheCLI

config :ethereumex, url: "http://127.0.0.1:8545"

config :money,
  # Ethereum API values in wei (lowest non-divisible unit in
  # Ethereum): 10^18 wei = 1 ETH
  custom_currencies: [
    WEI: %{
      name: "Wei",
      symbol: "WEI",
      exponent: 0
    },
    GWEI: %{
      name: "Giga Wei",
      symbol: "GWEI",
      exponent: 9
    },
    PWEI: %{
      name: "Peta Wei",
      symbol: "PWEI",
      exponent: 15
    },
    ETH: %{
      name: "Ethereumn",
      # symbol: "Ξ",
      symbol: "ETH",
      exponent: 18
    }
  ]

# import_config "#{Mix.env()}.exs"
