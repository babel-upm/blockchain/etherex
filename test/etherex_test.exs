defmodule EtherexTest do
  require Logger
  use ExUnit.Case

  test "up and running" do
    ## Until we find a client that syncs
    assert {:ok, nil} = Etherex.syncing()
    assert {:ok, _} = Etherex.client_version()
    assert {:ok, _} = Etherex.net_version()
    assert {:ok, _} = Etherex.protocol_version()
  end

  test "gas price" do
    assert {:ok, price} = Etherex.gas_price()
    assert is_integer(price)
    assert price > 0
    assert price == Etherex.gas_price!()
  end

  test "correct balances after transfer, gas included" do
    assert [a1, a2 | _] = Etherex.accounts!()

    for _ <- 1..10 do
      transfer_half_balance(a1, a2)
    end
  end

  defp transfer_half_balance(a1, a2) do
    assert bb1 = Etherex.get_balance!(a1)
    assert bb2 = Etherex.get_balance!(a2)
    value = div(bb1, 2)
    assert {:ok, hash} = Etherex.transfer(a1, a2, value)
    assert ab1 = Etherex.get_balance!(a1)
    assert ab2 = Etherex.get_balance!(a2)
    assert {:ok, _transaction} = Etherex.get_transaction(hash)
    assert {:ok, actual_gas_cost} = Etherex.gas_cost(hash)
    calculated_gas_cost = bb1 - value - ab1
    assert actual_gas_cost == calculated_gas_cost
    assert ab2 == bb2 + value
    # Returns balance consumed from a1
    value + actual_gas_cost
  end

  test "receipt of no transaction" do
    hash = "0xb903239f8543d04b5dc1ba6579132b143087c68db1b2168786408fcbce568238"
    assert nil == Etherex.get_transaction_receipt!(hash)
  end

  test "compile contract (non existent file)" do
    result = Etherex.compile_solidity("/tmp/non_existent_file.sol")

    assert result == {:error, "Compilation of Solidity via RPC is deprecated"} or
             result == {:error, "Method eth_compileSolidity not supported."} or
             result == {:error, "the method eth_compileSolidity does not exist/is not available"} or
             result == {:error, "Method deprecated"},
           "Unexpected result: #{inspect(result)}"
  end

  test "compile contract (source code)" do
    result =
      Etherex.compile_solidity(
        "contract test { function multiply(uint a) public returns(uint d) {return a * 7;} }"
      )

    assert result == {:error, "Compilation of Solidity via RPC is deprecated"} or
             result == {:error, "Method eth_compileSolidity not supported."} or
             result == {:error, "the method eth_compileSolidity does not exist/is not available"} or
             result == {:error, "Method deprecated"},
           "Unexpected result: #{inspect(result)}"
  end

  test "compile contract (contract name does not match)" do
    assert {:error, "Maybe, the name of the contract does not match the file name: Contador"} ==
             Etherex.compile_solidity("priv/solidity/Contador.sol")
  end

  test "compile contract (file)" do
    assert {:ok, _} = Etherex.compile_solidity("priv/solidity/Counter.sol")
  end

  test "compile contract (empty constructor)" do
    assert {:ok, _} = Etherex.compile_solidity("priv/solidity/Ping.sol")
  end

  test "deploy a contract" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    assert owner == Etherex.get_creator(instance)
    assert bytecode == Etherex.get_bytecode(instance)
    assert "0x" <> _hash = Etherex.get_contract_creation_hash(instance)
    assert {:ok, "0x" <> _address} = Etherex.get_contract_address(instance)
  end

  test "deploy a contract (empty constructor)" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Ping.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    assert owner == Etherex.get_creator(instance)
    assert bytecode == Etherex.get_bytecode(instance)
    assert "0x" <> _hash = Etherex.get_contract_creation_hash(instance)
    assert {:ok, "0x" <> _address} = Etherex.get_contract_address(instance)
  end

  test "deploy a contract and call some functions" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)
    assert {:ok, [0]} = Etherex.call(instance, owner, "get", [])
    assert {:ok, _} = Etherex.call(instance, owner, "inc", [])

    assert {:ok, _} = Etherex.call_transaction(instance, owner, "inc", [], gas: 1_000_000)

    assert {:ok, [1]} = Etherex.call(instance, owner, "get", [])
  end

  test "deploy a contract and call some functions which does not exist" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)
    assert {:error, _} = Etherex.call(instance, owner, "get_no_exists", [])
  end

  test "deploy a contract (empty constructor) and call some functions" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Ping.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)
    assert {:ok, _} = Etherex.call(instance, owner, "ping", [])
  end

  test "gas estimation" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    n = 10

    for _ <- 1..n do
      assert {:ok, estimated_gas_cost} = Etherex.estimate_gas_cost(instance, owner, "inc", [])
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "inc", [])
      assert {:ok, actual_gas_cost} = Etherex.gas_cost(hash)
      assert estimated_gas_cost == actual_gas_cost
    end

    ## Why n - 1? Because the gas cost estimation fails systematically if we run dec as many times as inc
    for _ <- 1..(n - 1) do
      assert {:ok, estimated_gas_cost} = Etherex.estimate_gas_cost(instance, owner, "dec", [])
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "dec", [])
      assert {:ok, actual_gas_cost} = Etherex.gas_cost(hash)
      assert estimated_gas_cost == actual_gas_cost
    end
  end

  test "get_balance does not consume gas" do
    assert [a1, a2 | _] = Etherex.accounts!()
    bb = Etherex.get_balance!(a1)

    for _ <- 1..10 do
      assert ^bb = Etherex.get_balance!(a1)
    end

    # Mainly to force a mining of a block
    consumed = transfer_half_balance(a1, a2)

    ab = Etherex.get_balance!(a1)

    assert ^consumed = bb - ab

    for _ <- 1..10 do
      assert ^ab = Etherex.get_balance!(a1)
    end
  end

  test "decode event from transaction" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    for i <- 1..10 do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "inc", [])
      assert {:ok, [{"Incremented", %{"v" => i}}]} == Etherex.get_events(instance, hash)
    end

    for i <- 1..10 do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "dec", [])
      assert {:ok, [{"Decremented", %{"v" => 10 - i}}]} == Etherex.get_events(instance, hash)
    end
  end

  test "decode event from call to another contract" do
    assert {:ok, ping_bytecode} = Etherex.compile_solidity("priv/solidity/Ping.sol")
    assert {:ok, pingpong_bytecode} = Etherex.compile_solidity("priv/solidity/PingPong.sol")

    assert {:ok, [owner | _]} = Etherex.accounts()
    assert {:ok, ping_instance} = Etherex.deploy(ping_bytecode, owner, [], gas: 1_000_000)
    assert {:ok, pingpong_instance} = Etherex.deploy(pingpong_bytecode, owner, [], gas: 1_000_000)

    assert {:ok, pingpong_hash} = Etherex.call_transaction(pingpong_instance, owner, "ping", [])
    assert {:ok, []} == Etherex.get_events(pingpong_instance, pingpong_hash)
    assert {:ok, [{"Msg", %{}}]} == Etherex.get_events(ping_instance, pingpong_hash)
  end

  test "code/decode arguments, events and returns" do
    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Store.sol")

    assert {:ok, [_a, _b, _c, _d, owner | _]} = Etherex.accounts()

    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [1], gas: 1_000_000)

    assert {:ok, [1]} == Etherex.call(instance, owner, "get", [])

    assert {:ok, hash} = Etherex.call_transaction(instance, owner, "set", [42])

    assert {:ok, [{"Set", %{"v" => 42}}]} == Etherex.get_events(instance, hash)

    assert {:ok, [42]} == Etherex.call(instance, owner, "get", [])
  end

  test "getting non existent transaction" do
    assert {:ok, nil} ==
             Etherex.get_transaction(
               "0x444172bef57ad978655171a8af2cfd89baa02a97fcb773067aef7794d6913374"
             )

    assert {:ok, nil} ==
             Etherex.get_transaction_receipt(
               "0x444172bef57ad978655171a8af2cfd89baa02a97fcb773067aef7794d6913374"
             )
  end

  test "information from existent transaction" do
    [_a, _b, c, d | _] = Etherex.accounts!()

    for i <- 1..10 do
      assert {:ok, hash1} = Etherex.transfer(c, d, i * 1_000_000)
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash1)
      assert {:ok, hash2} = Etherex.transfer(d, c, i * 1_000_000)
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash2)
    end
  end

  test "information from existent call transactions" do
    [_a, _b, _c, _d, owner | _] = Etherex.accounts!()

    assert {:ok, bytecode} = Etherex.compile_solidity("priv/solidity/Counter.sol")

    assert {:ok, instance} = Etherex.deploy(bytecode, owner, [], gas: 1_000_000)

    for _ <- 1..10 do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "inc", [])
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash)
    end

    for _ <- 1..10 do
      assert {:ok, hash} = Etherex.call_transaction(instance, owner, "dec", [])
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash)
    end

    for _ <- 1..10 do
      assert {:ok, hash1} = Etherex.call_transaction(instance, owner, "inc", [])
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash1)
      assert {:ok, hash2} = Etherex.call_transaction(instance, owner, "dec", [])
      assert {:ok, nil} != Etherex.get_transaction_receipt(hash2)
    end
  end
end
