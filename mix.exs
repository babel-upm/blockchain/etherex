defmodule Etherex.MixProject do
  use Mix.Project

  def project do
    [
      app: :etherex,
      version: "1.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      package: package(),
      name: "Etherex",
      source_url: "https://gitlab.com/babel-upm/blockchain/etherex"
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ethereumex, "~> 0.7.1"},
      {:ex_abi, "~> 0.6.0"},
      {:decimal, "~> 1.0"},
      {:money, "~> 1.4"},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.28.2", only: :dev, runtime: false}
    ]
  end

  defp description() do
    "Etherex is an Elixir high level library for the Ethereum blockchain based on the Ethereum Json RPC API"
  end

  defp package() do
    [
      files: ~w(lib priv .formatter.exs mix.exs README* LICENSE* CHANGELOG*),
      licenses: ["BSD-2-Clause"],
      links: %{"Gitlab" => "https://gitlab.com/babel-upm/blockchain/etherex"}
    ]
  end
end
