defmodule Etherex.Client.GanacheCLI do
  use Etherex.Client,
    command: "ganache-cli",
    args:
      "--defaultBalanceEther=1000000000000" <>
        " --gasPrice 1" <>
        " --gasLimit 9007199254740991" <>
        " --callGasLimit=0x1fffffffffffff"
end
