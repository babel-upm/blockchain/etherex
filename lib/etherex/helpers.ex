# Module with some helper functions related to encode/decode data
# to/from blockchain
defmodule Etherex.Helpers do
  @moduledoc false
  require Logger

  alias Etherex.Type

  @spec add_opts(map(), Keyword.t()) :: map()
  def add_opts(params, opts) do
    for {k, v} <- Enum.into(opts, params) do
      if is_integer(v) do
        {k, encode_quantity(v)}
      else
        {k, v}
      end
    end
    |> Enum.into(%{})
  end

  @spec decode_json_rpc_error({:error, :econnrefused}) :: {:error, :econnrefused}
  @spec decode_json_rpc_error({:error, :closed}) :: {:error, :closed}
  @spec decode_json_rpc_error({:error, :empty_respose}) :: {:error, :empty_response}
  @spec decode_json_rpc_error({:error, :timeout}) :: {:error, :timeout}
  @spec decode_json_rpc_error({:error, %{String.t() => any()}}) :: {:error, Type.call_error()}
  @spec decode_json_rpc_error({:error, {:invalid_json, Jason.DecodeError.t()}}) ::
          {:error, String.t()}
  @spec decode_json_rpc_error({:error, String.t()}) ::
          {:error, String.t()} | {:error, String.t(), Type.hash()}

  def decode_json_rpc_error(error) do
    decoded = decode_json_rpc_error2(error)
    Logger.debug("Decoded JsonRPC Error: #{inspect(decoded)} (#{inspect(error)})")
    decoded
  end

  defp decode_json_rpc_error2({:error, :econnrefused}), do: {:error, :econnrefused}
  defp decode_json_rpc_error2({:error, :closed}), do: {:error, :closed}
  defp decode_json_rpc_error2({:error, :empty_response}), do: {:error, :empty_response}
  defp decode_json_rpc_error2({:error, :timeout}), do: {:error, :timeout}

  # Case defined by inverse engineering from (mainly) ganache
  defp decode_json_rpc_error2({:error, %{"code" => code, "data" => data}})
       when is_integer(code) do
    # TODO: we assume in data there is only one error
    # [{hash, %{"error" => error, "reason" => reason}} | _] ->
    #   {:error, %{hash: hash, error: error, reason: reason}}

    case data do
      %{"name" => "c", "stack" => "c: " <> stack_trace} ->
        {:error, stack_trace |> String.split("\n") |> List.first()}

      %{"name" => "Error", "stack" => "Error: " <> stack_trace} ->
        {:error, stack_trace |> String.split("\n") |> List.first()}

      data when is_binary(data) ->
        {:error, data}

      _ ->
        Logger.error("data didn't match, error unknown: #{inspect(data)}")
        {:error, :unknown}
    end
    |> then(fn {error, result} ->
      hash =
        if is_map(data) do
          case Enum.find(data, fn {x, _} -> match?("0x" <> _value, x) end) do
            nil -> nil
            {hash, _} -> hash
          end
        else
          nil
        end

      if hash do
        {error, result, hash}
      else
        {error, result}
      end
    end)
  end

  # Case defined by inverse engineering from (mainly) openethereum
  defp decode_json_rpc_error2({:error, %{"code" => code, "message" => message}})
       when is_integer(code) do
    {:error, message}
  end

  defp decode_json_rpc_error2({:error, {:invalid_json, %Jason.DecodeError{data: message}}}) do
    {:error, message}
  end

  defp decode_json_rpc_error2({:error, message}) when is_binary(message) do
    {:error, message}
  end

  @spec encode_quantity(Type.quantity()) :: Type.hex()
  def encode_quantity(quantity) when is_integer(quantity) and quantity >= 0 do
    "0x" <>
      (quantity
       |> :binary.encode_unsigned()
       |> Base.encode16(case: :lower)
       # Some nodes (like ethnode) do not parse leading 0s in hex encoding
       |> case do
         "0" <> encoded -> encoded
         encoded -> encoded
       end)
  end

  @spec decode_quantity(Type.hex()) :: Type.quantity()
  def decode_quantity("0x" <> eth) do
    if rem(String.length(eth), 2) == 0 do
      eth
    else
      "0" <> eth
    end
    |> Base.decode16!(case: :lower)
    |> :binary.decode_unsigned()
  end

  @spec encode_tag(Type.tag()) :: String.t()
  def encode_tag(:earliest), do: "earliest"
  def encode_tag(:latest), do: "latest"
  def encode_tag(:pending), do: "pending"

  @spec encode_block_parameter(Type.block_parameter()) :: String.t() | Type.hex()
  def encode_block_parameter(tag) when is_atom(tag) do
    encode_tag(tag)
  end

  def encode_block_parameter(block_number) when is_integer(block_number) do
    encode_quantity(block_number)
  end
end
