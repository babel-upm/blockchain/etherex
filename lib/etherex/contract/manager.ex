defmodule Etherex.Contract.Manager do
  use GenServer

  require Logger

  @moduledoc """
  This module provides an abstraction layer over the default Etherex contract. This abstraction
  allows the user to interact with contracts only using addresses and transaction hashes.
  """

  ##############################################################################
  # types
  ##############################################################################

  @type address() :: Etherex.address()
  @type contract() :: Etherex.contract()
  @type bytecode() :: Etherex.bytecode()
  @type opts() :: Etherex.opts()
  @type error() :: Etherex.error()
  @type block_parameter :: Etherex.block_parameter()
  @type hash() :: Etherex.hash()
  @type event() :: Etherex.event()

  ##############################################################################
  # API
  ##############################################################################

  @doc """
  Starts the contract manager process.
  """
  @spec start() :: {:ok, pid()}
  def start() do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc """
  Stops the contract manager process.
  """
  @spec stop() :: :ok
  def stop(reason \\ :normal, timeout \\ :infinity) do
    GenServer.stop(__MODULE__, reason, timeout)
  end

  @doc """
  Compiles and registers a contract in the process.
  """
  @spec register(name :: atom(), path :: Path.t()) :: :ok
  def register(name, path) do
    GenServer.call(__MODULE__, {:register, {name, path}})
  end

  @doc """
  Deploys a contract of a given type in the blockchain. This contract should be previously
  registered. Returns the transaction in which the contract will be deployed.
  """
  @spec deploy(name :: atom(), caller :: address(), arguments :: list(), opts()) ::
          {:ok, hash()} | {:error, error()}
  def deploy(name, caller, arguments, opts \\ []) do
    GenServer.call(__MODULE__, {:deploy, {name, caller, fix_arguments(arguments), opts}})
  end

  @doc """
  Retrieves the address of the given transaction if it is already in a mined.
  """
  @spec get_address(hash()) :: {:ok, address()} | {:error, error()}
  def get_address(hash) do
    GenServer.call(__MODULE__, {:get_address, {hash}})
  end

  @doc """
  Given the registered name, retrieves the contract information once compiled.
  """
  @spec get_bytecode(atom()) :: {:ok, bytecode()} | {:error, error()}
  def get_bytecode(name) do
    GenServer.call(__MODULE__, {:get_bytecode, {name}})
  end

  @doc """
  Performs a call to a function of a deployed contract. This call does not generate a transaction in
  the blockchain.
  """
  @spec call(
          contract :: address(),
          caller :: address(),
          function :: atom(),
          arguments :: list(),
          opts()
        ) :: {:ok, any()} | {:error, error()}
  def call(contract, caller, function, arguments, opts \\ []) do
    GenServer.call(
      __MODULE__,
      {:call, {contract, caller, Atom.to_string(function), fix_arguments(arguments), opts}}
    )
  end

  @doc """
  Performs a call to a function of a deployed contract, This call generates a transaction in the
  blockchain.
  """
  @spec call_transaction(
          contract :: address(),
          caller :: address(),
          function :: atom(),
          list(),
          opts()
        ) ::
          {:ok, hash()} | {:error, error()}
  def call_transaction(contract, caller, function, arguments, opts \\ []) do
    GenServer.call(
      __MODULE__,
      {:call_transaction,
       {contract, caller, Atom.to_string(function), fix_arguments(arguments), opts}}
    )
  end

  @doc """
  Given a transaction, retrieves the list of events logged in it for all known contracts.
  Returns `[]` if no events. Returns `nil` if transaction is pending.

  Events are not returned in order.
  """
  @spec get_events(hash) :: {:ok, [event()] | nil} | {:error, error()}
  def get_events(hash) do
    GenServer.call(__MODULE__, {:get_events, {hash}})
  end

  @doc """
  """
  @spec get_revert_reason(hash()) :: {:ok, String.t()} | {:error, error()}
  def get_revert_reason(hash) do
    GenServer.call(__MODULE__, {:get_revert_reason, {hash}})
  end

  ##############################################################################

  @type call_transaction :: %{
          caller: address(),
          function: String.t(),
          arguments: []
        }
  @type state() :: %{
          contracts: MapSet.t(atom()),
          deployed: MapSet.t(address()),
          errors: %{hash() => String.t()}
        }

  @spec init([]) :: {:ok, state()}
  def init([]) do
    :ets.new(__MODULE__, [:named_table, :set])
    initial_state = %{contracts: MapSet.new(), deployed: MapSet.new(), errors: Map.new()}
    {:ok, initial_state}
  end

  ##############################################################################

  @spec handle_call(
          {:register, {atom(), Path.t()}}
          | {:deploy, {atom(), address(), list(), opts()}}
          | {:get_address, {hash()}}
          | {:call, {address(), address(), String.t(), list(), opts()}}
          | {:call_transaction, {address(), address(), String.t(), list(), opts()}}
          | {:get_bytecode, {atom()}}
          | {:get_events, {hash()}},
          {pid(), any()},
          state
        ) :: {:reply, :ok | {:error, error()}, state}

  def handle_call({:register, {name, path}}, _from, state) do
    with {:ok, bytecode} <- Etherex.compile_solidity(path),
         true <- :ets.insert(__MODULE__, {name, bytecode}) do
      new_state = %{state | contracts: MapSet.put(state.contracts, name)}
      {:reply, :ok, new_state}
    else
      false -> {:reply, {:error, "#{__MODULE__} not started"}, state}
      error -> {:reply, error, state}
    end
  end

  def handle_call({:deploy, {name, caller, arguments, opts}}, _from, state) do
    with [{^name, bytecode}] <- :ets.lookup(__MODULE__, name),
         {:ok, contract} <- Etherex.deploy(bytecode, caller, arguments, opts),
         hash <- Etherex.get_contract_creation_hash(contract),
         true <- :ets.insert(__MODULE__, {hash, contract}) do
      {:reply, {:ok, hash}, state}
    else
      [] ->
        {:reply, "bytecode for contract #{inspect(name)} not found", state}

      false ->
        {:reply, "#{__MODULE__} not started", state}

      {:error, reason, hash} ->
        state = %{state | errors: Map.put(state.errors, hash, reason)}
        {:reply, {:ok, hash}, state}

      error ->
        Logger.error("unknown deploy error: #{inspect(error)}")
        {:reply, error, state}
    end
  end

  def handle_call({:get_address, {hash}}, _from, state) do
    with [{^hash, contract}] <- :ets.lookup(__MODULE__, hash),
         {:ok, address} <- Etherex.get_contract_address(contract) do
      if is_nil(address) do
        {:reply, {:ok, nil}, state}
      else
        :ets.insert(__MODULE__, {address, contract})
        new_state = %{state | deployed: MapSet.put(state.deployed, address)}
        {:reply, {:ok, address}, new_state}
      end
    else
      error -> {:reply, error, state}
    end
  end

  def handle_call(
        {:call, {address, caller, function, arguments, opts}},
        _from,
        state
      ) do
    with [{^address, contract}] <- :ets.lookup(__MODULE__, address),
         {:ok, result} <- Etherex.call(contract, caller, function, arguments, :latest, opts) do
      {:reply, {:ok, result}, state}
    else
      [] -> {:reply, {:error, "contract #{address} not found"}, state}
      error -> {:reply, error, state}
    end
  end

  def handle_call(
        {:call_transaction, {address, caller, function, arguments, opts}},
        _from,
        state
      ) do
    with [{^address, contract}] <- :ets.lookup(__MODULE__, address),
         {:ok, hash} <- Etherex.call_transaction(contract, caller, function, arguments, opts) do
      {:reply, {:ok, hash}, state}
    else
      [] ->
        {:reply, {:error, "contract #{address} not found"}, state}

      {:error, reason, hash} ->
        state = %{state | errors: Map.put(state.errors, hash, reason)}
        {:reply, {:ok, hash}, state}

      error ->
        {:reply, error, state}
    end
  end

  def handle_call({:get_bytecode, {name}}, _from, state) do
    with [{^name, bytecode}] <- :ets.lookup(__MODULE__, name) do
      {:reply, {:ok, bytecode}, state}
    else
      [] ->
        {:reply, "bytecode for contract #{inspect(name)} not found", state}

      error ->
        Logger.error("unkonwn get_bytecode error: #{inspect(error)}")
        {:reply, error, state}
    end
  end

  def handle_call({:get_events, {hash}}, _from, state) do
    is_transaction_pending =
      Etherex.get_transaction_receipt(hash)
      |> elem(1)
      |> is_nil()

    if is_transaction_pending do
      {:reply, {:ok, nil}, state}
    else
      # Select by is_atom because all bytecodes are linked like %{atom() => bytecode()}.
      all_bytecodes = :ets.select(__MODULE__, [{{:"$1", :"$2"}, [{:is_atom, :"$1"}], [:"$2"]}])

      events =
        for bytecode <- all_bytecodes do
          case Etherex.get_events(bytecode, hash) do
            {:ok, events} -> events
          end
        end
        |> Enum.concat()

      {:reply, {:ok, events}, state}
    end
  end

  def handle_call({:get_revert_reason, {hash}}, _from, state) do
    case Map.fetch(state.errors, hash) do
      {:ok, reason} -> {:reply, {:ok, reason}, state}
      :error -> {:reply, {:error, "hash #{hash} not found"}, state}
    end
  end

  ##############################################################################
  # Helpers
  ##############################################################################

  # TODO move this function Etherex
  defp fix_arguments(args), do: Enum.map(args, &fix_argument/1)

  defp fix_argument("0x" <> data), do: data |> Base.decode16!(case: :lower)
  defp fix_argument(data), do: data
end
