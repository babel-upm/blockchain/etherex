defmodule Etherex.Client.Openethereum do
  use Etherex.Client, command: "openethereum", args: "--config dev", killall: "openethereum"
end
