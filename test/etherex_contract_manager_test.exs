defmodule EtherexContractManagerTest do
  use ExUnit.Case

  alias Etherex.Contract.Manager

  require Logger

  test "start and stop" do
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.stop()
  end

  test "deploy a contract" do
    assert {:ok, [owner | _]} = Etherex.accounts()
    # starts the contract manager and registers a contract
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.register(Counter, "priv/solidity/Counter.sol")
    # deploys a contract
    assert {:ok, hash} = Manager.deploy(Counter, owner, [], gas: 1_000_000)
    assert {:ok, _address} = Manager.get_address(hash)

    Manager.stop()
  end

  test "deploys a counter, increments it and retrieves the event" do
    assert {:ok, [owner | _]} = Etherex.accounts()
    # starts the contract manager and registers a contract
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.register(Counter, "priv/solidity/Counter.sol")
    # deploys a counter
    assert {:ok, hash} = Manager.deploy(Counter, owner, [], gas: 1_000_000)
    assert {:ok, address} = Manager.get_address(hash)
    # increments the counter
    assert {:ok, hash} = Manager.call_transaction(address, owner, :inc, [], gas: 1_000_000)
    assert {:ok, [{"Incremented", %{"v" => 1}}]} = Manager.get_events(hash)

    Manager.stop()
  end

  test "deploys contracts, gets all events from all contracts" do
    assert {:ok, [owner | _]} = Etherex.accounts()
    # starts the contract manager and registers the contracts
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.register(Ping, "priv/solidity/Ping.sol")
    assert :ok = Manager.register(PingPong, "priv/solidity/PingPong.sol")
    # deploys the contracts
    assert {:ok, ping_hash} = Manager.deploy(Ping, owner, [], gas: 1_000_000)
    assert {:ok, pingpong_hash} = Manager.deploy(PingPong, owner, [], gas: 1_000_000)
    assert {:ok, ping_address} = Manager.get_address(ping_hash)
    assert {:ok, pingpong_address} = Manager.get_address(pingpong_hash)
    # increments the counter
    assert {:ok, hash} =
             Manager.call_transaction(pingpong_address, owner, :ping, [], gas: 1_000_000)

    assert {:ok, [{"Msg", %{}}]} = Manager.get_events(hash)

    Manager.stop()
  end

  test "deploys contracts, gets all events from all contracts. Multiple times deployed different owners" do
    assert {:ok, [owner | [owner2 | _]]} = Etherex.accounts()
    # starts the contract manager and registers the contracts
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.register(Ping, "priv/solidity/Ping.sol")
    assert :ok = Manager.register(PingPong, "priv/solidity/PingPong.sol")
    # deploys the contracts
    assert {:ok, ping_hash} = Manager.deploy(Ping, owner, [], gas: 1_000_000)
    assert {:ok, pingpong_hash} = Manager.deploy(PingPong, owner, [], gas: 1_000_000)
    assert {:ok, ping_address} = Manager.get_address(ping_hash)
    assert {:ok, pingpong_address} = Manager.get_address(pingpong_hash)
    # Deploy again
    assert {:ok, ping_hash_2} = Manager.deploy(Ping, owner2, [], gas: 1_000_000)
    assert {:ok, pingpong_hash_2} = Manager.deploy(PingPong, owner2, [], gas: 1_000_000)
    assert {:ok, _ping_address_2} = Manager.get_address(ping_hash_2)
    assert {:ok, _pingpong_address_2} = Manager.get_address(pingpong_hash_2)
    # increments the counter
    assert {:ok, hash} =
             Manager.call_transaction(pingpong_address, owner, :ping, [], gas: 1_000_000)

    assert {:ok, [{"Msg", %{}}]} = Manager.get_events(hash)

    Manager.stop()
  end

  test "deploys contracts, gets all events from all contracts. Multiple times deployed same owner" do
    assert {:ok, [owner | [owner2 | _]]} = Etherex.accounts()
    # starts the contract manager and registers the contracts
    assert {:ok, _pid} = Manager.start()
    assert :ok = Manager.register(Ping, "priv/solidity/Ping.sol")
    assert :ok = Manager.register(PingPong, "priv/solidity/PingPong.sol")
    # deploys the contracts
    assert {:ok, ping_hash} = Manager.deploy(Ping, owner, [], gas: 1_000_000)
    assert {:ok, pingpong_hash} = Manager.deploy(PingPong, owner, [], gas: 1_000_000)
    assert {:ok, ping_address} = Manager.get_address(ping_hash)
    assert {:ok, pingpong_address} = Manager.get_address(pingpong_hash)
    # Deploy again
    assert {:ok, ping_hash_2} = Manager.deploy(Ping, owner, [], gas: 1_000_000)
    assert {:ok, pingpong_hash_2} = Manager.deploy(PingPong, owner, [], gas: 1_000_000)
    assert {:ok, _ping_address_2} = Manager.get_address(ping_hash_2)
    assert {:ok, _pingpong_address_2} = Manager.get_address(pingpong_hash_2)
    # increments the counter
    assert {:ok, hash} =
             Manager.call_transaction(pingpong_address, owner, :ping, [], gas: 1_000_000)

    assert {:ok, [{"Msg", %{}}]} = Manager.get_events(hash)

    Manager.stop()
  end
end
