defmodule Etherex.Contract do
  @moduledoc """
  Types and functions related to bytecode and contracts.
  """

  require Logger

  alias Ethereumex.HttpClient, as: JsonRPC

  import Etherex.Helpers,
    only: [add_opts: 2, decode_json_rpc_error: 1, decode_quantity: 1, encode_block_parameter: 1]

  alias Etherex.Type

  @typedoc """
  Bytecode and specification of a contract.
  """
  @type bytecode() :: %{
          code: String.t(),
          interface: [ABI.FunctionSelector.t()]
        }

  @typedoc """
  Data of the instance of a contract deployed in a blockchain.
  """
  @opaque contract() :: %{
            creator: Type.address(),
            bytecode: bytecode(),
            hash: Type.hash(),
            address: Type.address() | nil
          }

  @spec is_contract?(contract() | map()) :: boolean()
  def is_contract?(%{creator: _, bytecode: _, hash: _, address: _}), do: true
  def is_contract?(_), do: false

  @doc """
  Given a contract, returns its creator address.
  """
  @spec get_creator(contract()) :: Type.address()
  def get_creator(%{creator: creator}), do: creator

  @doc """
  Given a contract, returns its deployed bytecode.
  """
  @spec get_bytecode(contract()) :: bytecode()
  def get_bytecode(%{bytecode: bytecode}), do: bytecode

  @doc """
  Given a contract, returns the contract creation transaction hash.
  """
  @spec get_creation_hash(contract()) :: Type.hash()
  def get_creation_hash(%{hash: hash}), do: hash

  @doc """
  Given a contract, returns its address if the creation transaction has already been mined.
  """
  @spec get_address(contract()) :: {:ok, Type.address() | nil} | {:error, Type.error()}
  def get_address(%{address: nil, hash: hash}), do: get_contract_address(hash)
  def get_address(%{address: address}), do: {:ok, address}

  @doc """
  See Etherex.compile_solidity/1.
  """
  @spec compile_solidity(String.t()) :: {:ok, bytecode()} | {:error, Type.error()}
  def compile_solidity(source_or_filename) do
    case File.read(source_or_filename) do
      {:error, _} ->
        throw(source_or_filename)

      {:ok, source} ->
        filename = source_or_filename
        dirname = Path.dirname(filename)
        basename = Path.basename(filename, ".sol")
        binname = "#{dirname}/#{basename}.bin"
        abiname = "#{dirname}/#{basename}.abi"

        Logger.info("Compiling #{filename} with 'solc'")

        try do
          {_, 0} =
            System.cmd("solc", [
              "--optimize",
              "--bin",
              "--abi",
              "--overwrite",
              filename,
              "-o",
              dirname
            ])

          case File.read(binname) do
            {:error, _} ->
              {:error,
               "Maybe, the name of the contract does not match the file name: #{basename}"}

            {:ok, bytecode} ->
              {:ok, abicode} = File.read(abiname)
              {:ok, interface} = Jason.decode(abicode)
              create_bytecode(bytecode, interface)
          end
        rescue
          _ ->
            Logger.error("solc failed to compile #{filename}")
            throw(source)
        end
    end
  catch
    source ->
      case JsonRPC.eth_compile_solidity(source) do
        {:ok, compiled} ->
          bytecode = compiled["code"]
          interface = compiled["info"]["interface"]
          create_bytecode(bytecode, interface)

        error ->
          decode_json_rpc_error(error)
      end
  end

  @doc """
  See Etherex.deploy/3.
  """
  @spec deploy(
          bytecode :: bytecode(),
          creator :: Type.address(),
          arguments :: list(),
          opts :: Keyword.t()
        ) :: {:ok, contract()} | {:error, Type.error()}
  def deploy(bytecode, creator, arguments, opts \\ []) do
    constructor_spec = get_constructor_spec(bytecode)

    arity =
      if is_nil(constructor_spec) do
        0
      else
        length(constructor_spec.types)
      end

    nargs = length(arguments)

    if nargs == arity do
      # Solidity allows contracts without constructors, equivalent to `constructor () {}`
      # This behaviour makes `get_constructor_spec/1` to return `nil` if not defined.
      constructor_spec_types =
        if is_nil(constructor_spec) do
          []
        else
          constructor_spec.types
        end

      decoded_arguments =
        List.zip([arguments, constructor_spec_types])
        |> Enum.map(fn {val, type} ->
          ABI.TypeEncoder.encode_raw([val], [type]) |> Base.encode16(case: :lower)
        end)
        |> Enum.reduce("", &(&2 <> &1))

      %{from: creator, data: "0x" <> bytecode.code <> decoded_arguments}
      |> add_opts(opts)
      |> JsonRPC.eth_send_transaction()
      |> case do
        {:ok, hash} ->
          {:ok,
           %{
             creator: creator,
             bytecode: bytecode,
             hash: hash,
             address: get_contract_address!(hash)
           }}

        error ->
          decode_json_rpc_error(error)
      end
    else
      {:error,
       "Incorrect number of args calling constructor: #{nargs} args but arity is #{arity}"}
    end
  end

  @doc """
  See Etherex.call/5
  """
  @spec call(
          contract :: contract(),
          caller :: Type.address(),
          function :: String.t(),
          arguments :: list(),
          block_or_opts :: Type.block_parameter() | Type.opts(),
          opts :: Type.opts()
        ) :: {:ok, any()} | {:error, Type.error()}
  def call(contract, caller, function, arguments, block_or_opts \\ :latest, opts \\ []) do
    {block, opts} =
      if is_list(block_or_opts) and opts == [] do
        {:latest, block_or_opts}
      else
        {block_or_opts, opts}
      end

    with {:ok, call_data} <- create_call(contract, caller, function, arguments, opts),
         function_spec <- get_function_spec(contract.bytecode, function),
         {:ok, "0x" <> value} <- JsonRPC.eth_call(call_data, encode_block_parameter(block)) do
      # TODO: decoding can fail, maybe we need our own decode_raw that
      # returns a proper error instead of an exception?
      decoded_value =
        value
        |> Base.decode16!(case: :lower)
        |> ABI.TypeDecoder.decode_raw(function_spec.returns)

      {:ok, decoded_value}
    else
      error ->
        decode_json_rpc_error(error)
    end
  end

  @doc """
  Seel Etherex.call_transaction/4.
  """
  @spec call_transaction(
          contract :: contract(),
          caller :: Type.address(),
          function :: String.t(),
          arguments :: list(),
          opts :: Type.opts()
        ) :: {:ok, Type.hash()} | {:error, Type.error()}
  def call_transaction(contract, caller, function, arguments, opts \\ []) do
    with {:ok, call_data} <- create_call(contract, caller, function, arguments, opts),
         {:ok, hash} <- JsonRPC.eth_send_transaction(call_data) do
      {:ok, hash}
    else
      error ->
        decode_json_rpc_error(error)
    end
  end

  @doc """
  See Etherex.estimate_gas/4.
  """
  @spec estimate_gas(
          contract :: contract(),
          caller :: Type.address(),
          function :: String.t(),
          arguments :: list(),
          opts :: Type.opts()
        ) :: {:ok, Type.quantity()} | {:error, Type.error()}
  def estimate_gas(contract, caller, function, arguments, opts \\ []) do
    with {:ok, call_data} <- create_call(contract, caller, function, arguments, opts),
         {:ok, value} <- JsonRPC.eth_estimate_gas(call_data, opts) do
      {:ok, decode_quantity(value)}
    else
      error ->
        decode_json_rpc_error(error)
    end
  end

  @doc """
  See Etherex.get_events/2.
  """

  @spec get_events(bytecode_or_contract :: bytecode() | contract(), hash :: Type.hash()) ::
          {:ok, [Type.event()]}
  def get_events(bytecode_or_contract, hash) do
    bytecode =
      if is_contract?(bytecode_or_contract) do
        get_bytecode(bytecode_or_contract)
      else
        bytecode_or_contract
      end

    case Etherex.get_transaction_receipt(hash) do
      {:ok, nil} ->
        {:ok, []}

      {:ok, %{"logs" => logs}} ->
        log_to_event = fn log ->
          "0x" <> topic = hd(log["topics"])
          "0x" <> data = log["data"]

          ABI.Event.find_and_decode(
            bytecode.interface,
            topic |> Base.decode16!(case: :lower),
            nil,
            nil,
            nil,
            data |> Base.decode16!(case: :lower)
          )
          |> case do
            {:error, _error} ->
              # TODO(AH): in general, this error means that the
              # event in the log is an event of a different
              # contract
              # IB: We have included an event decoding with every registered contract,
              # usefull for nested calls/logs in contracts calling other contracts.
              # Previouslly there was a log call:
              #   Logger.error("Error in ABI.Event.find_and_decode: #{inspect(error)}")
              :error

            {spec, values} ->
              values =
                for {name, _type, _indexed, value} <- values, reduce: %{} do
                  acc when is_binary(value) ->
                    Map.put(acc, name, "0x" <> Base.encode16(value, case: :lower))

                  acc ->
                    Map.put(acc, name, value)
                end

              {:ok, {spec.function, values}}
          end
        end

        events =
          for log <- logs,
              event_or_error = log_to_event.(log),
              match?({:ok, _event}, event_or_error),
              {:ok, event} = event_or_error do
            event
          end

        {:ok, events}
    end
  end

  ######################################################################
  ## Helpers
  @spec create_bytecode(String.t(), map()) :: {:ok, bytecode()} | {:error, Type.error()}
  defp create_bytecode(bytecode, interface) do
    try do
      {:ok,
       %{code: bytecode, interface: ABI.parse_specification(interface, include_events?: true)}}
    rescue
      _ ->
        {:error,
         "Cannot create bytecode with code #{inspect(bytecode)} and ABI #{inspect(interface)}"}
    end
  end

  @spec get_constructor_spec(bytecode()) :: ABI.FunctionSelector.t() | nil
  def get_constructor_spec(bytecode) do
    Enum.find(bytecode.interface, nil, &(&1.type == :constructor))
  end

  @spec get_function_spec(bytecode(), String.t()) :: ABI.FunctionSelector.t() | nil
  def get_function_spec(bytecode, name) do
    Enum.find(bytecode.interface, nil, &(&1.function == name && &1.type == :function))
  end

  @spec get_event_specs(bytecode()) :: [ABI.FunctionSelector.t()]
  def get_event_specs(bytecode) do
    Enum.filter(bytecode.interface, &(&1.type == :event))
  end

  defp create_call(contract, caller, function, arguments, opts) do
    function_spec = get_function_spec(contract.bytecode, function)

    arity =
      if is_nil(function_spec) do
        0
      else
        length(function_spec.types)
      end

    nargs = length(arguments)

    cond do
      is_nil(function_spec) ->
        {:error, "Function \"#{function}/#{nargs}\" not found"}

      nargs != arity ->
        {:error,
         "Incorrect number of args calling \"#{function}\": #{nargs} args but arity is #{arity}"}

      nargs == arity ->
        call_data =
          function_spec
          |> ABI.FunctionSelector.encode()
          |> ABI.encode(arguments)
          |> Base.encode16(case: :lower)
          |> (&%{from: caller, to: contract.address, data: "0x" <> &1}).()
          |> add_opts(opts)

        {:ok, call_data}
    end
  end

  @spec get_contract_address(Type.hash()) :: {:ok, Type.address() | nil} | {:error, Type.error()}
  def get_contract_address(hash) do
    case Etherex.get_transaction_receipt(hash) do
      {:ok, nil} ->
        {:ok, nil}

      {:ok, receipt} ->
        {:ok, receipt["contractAddress"]}

      error ->
        error
    end
  end

  @spec get_contract_address!(Type.hash()) :: Type.address() | nil
  defp get_contract_address!(hash) do
    case get_contract_address(hash) do
      {:ok, address} -> address
      {:error, _error} -> nil
    end
  end
end
